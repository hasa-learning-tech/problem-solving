#Even Odd Problem Solving using Bitwise and Operator

#Input -> Integer
#ouput -> Statement Along with Input

#Algorithm:
#Step 1: Input -> Integer
#Step 2: Condition Checking
#Step 3: Output

#Logic : 
""" 
input -> 5 -> 5 & 1  == 1 -> odd

8 4 2 1                     0 and 1 =0
0 1 0 1 		    1 and 1 = 1
			    0 and 0  =0
			    1 and 0  =0
			    
8 4 2 1                    8 4 2 1
0 1 0 1		   0 1 1 0
0 0 0 1		   0 0 0 1
--------		-------------
0 0 0 1 -> 1		   0 0 0 0 -> 0
"""	

num=int(input("Enter a Number"))
if num&1==1:
	print(f"{num} is odd number")
else:
	print(f"{num} is Even Number")

		    
