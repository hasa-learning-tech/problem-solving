#Even Or Odd in Python

#input - Integer
#output - Statement with input

#Algorithm:
#Step1: Input -> Integer
#Step2: Condition Check
#Step3: Output


num=int(input("Enter a Number ->"))
if num%2==0:
	print(f'{num} is Even Number')
else:
	print(f'{num} is Odd Number')
