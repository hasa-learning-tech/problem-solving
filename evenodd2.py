#Even odd Program Using Multiplication and Division Method

#Input -> Integer
#Output -> Statement with Input Number

#Algorithm:

#Step1: Input -> Integer
#Step2: Condition Check
#Step3 : Output

#Even Logic: 8/2 = 4 -> 4*2 ==8 Even Number
#Odd Logic : 9/2 = 4 -> 4*2 ==9 Odd Number


num=int(input("Enter a Number ->"))
if (num//2)*2==num:
	print(f'{num} is Even Number')
else:
	print(f'{num} is Odd Number')






  
