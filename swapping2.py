first=int(input("Enter The First Number :-"))
second=int(input("Enter The Second Number:-"))

print(f'Before Swapping First: {first} Second:{second}')

first=first+second      # 5+4 =9
second=first-second     # 9-4 =5
first=first-second      # 9-5 =4

print(f'After Swapping First:{first} Second:{second}')
