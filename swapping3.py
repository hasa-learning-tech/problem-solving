first=int(input("Enter the First Number:-"))
second=int(input("Enter the Second Number:-"))

print(f'Before swapping First:{first} Second:{second}')

first=first^second      #1   
second=first^second     #5
first=first^second    	#4   

print(f'After swapping First:{first} Second:{second}')



#logic
#8 4 2 1      8 4 2 1          8 4 2 1
#0 1 0 1      0 0 0 1          0 0 0 1
#0 1 0 0      0 1 0 0          0 1 0 1   
#0 0 0 1      0 1 0 1 ->5      0 1 0 0->4
